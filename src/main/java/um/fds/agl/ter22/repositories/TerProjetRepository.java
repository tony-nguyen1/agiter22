package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.TerProjet;

public interface TerProjetRepository extends CrudRepository<TerProjet, Long> {
//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    TerProjet save(@Param("terProjet") TerProjet terProjet);
}
