package um.fds.agl.ter22.forms;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class TerProjetForm {

    private long id;
    private String titre;
    private String description;

    public TerProjetForm() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
