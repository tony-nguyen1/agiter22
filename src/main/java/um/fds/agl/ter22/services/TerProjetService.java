package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.TerProjet;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.repositories.TerProjetRepository;

import java.util.Optional;

@Service
public class TerProjetService {

    @Autowired
    private TerProjetRepository terProjetRepository;

    public Optional<TerProjet> getTerProjet(final Long id) {
        return terProjetRepository.findById(id);
    }

    public Iterable<TerProjet> getTerProjets() {
        return terProjetRepository.findAll();
    }

//    public void deleteTeacher(final Long id) {
//        teacherRepository.deleteById(id);
//    }
//
    public TerProjet saveTerProjet(TerProjet terProjet) {
        TerProjet savedTerProjet = terProjetRepository.save(terProjet);
        return savedTerProjet;
    }

    public Optional<TerProjet> findById(long id) {
        return terProjetRepository.findById(id);
    }

}
