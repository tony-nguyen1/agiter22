package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.TerProjet;
import um.fds.agl.ter22.forms.TeacherForm;
import um.fds.agl.ter22.forms.TerProjetForm;
import um.fds.agl.ter22.services.TeacherService;
import um.fds.agl.ter22.services.TerProjetService;

@Controller
public class TerProjetController implements ErrorController {

    @Autowired
    private TerProjetService terProjetService;

    @GetMapping("/listTerProjets")
    public Iterable<TerProjet> getTerProjets(Model model) {
        Iterable<TerProjet> terProjets=terProjetService.getTerProjets();
        model.addAttribute("terProjets", terProjets);
        return terProjets;
    }

    @GetMapping("/test")
    public Iterable<TerProjet> getT(Model model) {
        return null;
    }

    @GetMapping(value = { "/addTerProjet" })
    public String showAddTeacherPage(Model model) {

        TerProjetForm terProjetForm = new TerProjetForm();
        model.addAttribute("terProjetForm", terProjetForm);

        return "addTerProjet";
    }

    @PostMapping(value = { "/addTerProjet"})
    public String addTeacher(Model model, @ModelAttribute("TerProjetForm") TerProjetForm terProjetForm) {
        TerProjet t;
        //terProjetService
        /*if(teacherService.findById(teacherForm.getId()).isPresent()){
            // teacher already existing : update
            t = teacherService.findById(teacherForm.getId()).get();
            t.setFirstName(teacherForm.getFirstName());
            t.setLastName(teacherForm.getLastName());
        } else {
            // teacher not existing : create
            t=new Teacher(teacherForm.getFirstName(), teacherForm.getLastName(), terManagerService.getTERManager());
        }
        teacherService.saveTeacher(t);*/

        t = new TerProjet(terProjetForm.getTitre(), terProjetForm.getDescription());
        terProjetService.saveTerProjet(t);

        return "redirect:/listTerProjets";

    }


}
